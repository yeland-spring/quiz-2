package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.time.ZonedDateTime;

public class CreateReservationRequest {
    @NotNull
    @Size(max = 128)
    private String username;
    @NotNull
    @Size(max = 64)
    private String companyName;
    @NotNull
    private String zoneId;
    @NotNull
    private ZonedDateTime startTime;
    @NotNull
    @Pattern(regexp = "^PT[1-3]H$")
    private Duration duration;

    public CreateReservationRequest(String username, String companyName, String zoneId, ZonedDateTime startTime, Duration duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }
}
