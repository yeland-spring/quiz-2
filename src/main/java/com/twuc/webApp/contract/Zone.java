package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class Zone {
    @NotNull
    private String zoneId;

    public Zone() {
    }

    public Zone(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
