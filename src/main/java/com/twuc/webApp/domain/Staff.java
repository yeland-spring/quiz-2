package com.twuc.webApp.domain;

import com.twuc.webApp.contract.CreateStaffRequest;

import javax.persistence.*;
import java.time.ZoneId;

@Entity
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 64)
    private String firstName;
    @Column(nullable = false, length = 64)
    private String lastName;
    @Column
    private ZoneId zoneId;

    public Staff(CreateStaffRequest staffRequest) {
        this.firstName = staffRequest.getFirstName();
        this.lastName = staffRequest.getLastName();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }
}
