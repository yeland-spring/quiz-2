package com.twuc.webApp.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.zone.ZoneRulesException;

@ControllerAdvice
public class StaffControllerExceptionHandler {
    @ExceptionHandler({ZoneRulesException.class})
    ResponseEntity<Void> handleZoneIdException(ZoneRulesException exception) {
        return ResponseEntity.status(400).build();
    }
}
