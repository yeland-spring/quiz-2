package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import com.twuc.webApp.contract.Zone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestController
@RequestMapping("/api")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @PostMapping("/staffs")
    public ResponseEntity<Void> createStaff(@RequestBody CreateStaffRequest staffRequest) {
        Staff staff = new Staff(staffRequest);
        Staff savedStaff = staffRepository.save(staff);
        String uri = linkTo(methodOn(this.getClass()).createStaff(staffRequest)).toString() + "/" + savedStaff.getId();
        return ResponseEntity.status(HttpStatus.CREATED).header("location", uri).build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity<Staff> getStaff(@PathVariable Long staffId) {
        boolean present = staffRepository.findById(staffId).isPresent();
        if (present) {
            Staff staff = staffRepository.findById(staffId).get();
            return ResponseEntity.ok(staff);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping("/staffs")
    public ResponseEntity<List<Staff>> getStaffs() {
        List<Staff> staffs = staffRepository.findAll();
        return ResponseEntity.ok(staffs);
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity<Void> setTimeZone(@PathVariable Long staffId, @RequestBody @Valid Zone zone) {
        ZoneId zoneId = ZoneId.of(zone.getZoneId());
        Staff staff = staffRepository.findById(staffId).get();
        staff.setZoneId(zoneId);
        staffRepository.flush();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/timezones")
    public ResponseEntity<List<String>> getTimezones() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> timezones = availableZoneIds.stream().sorted().collect(Collectors.toList());
        return ResponseEntity.ok(timezones);
    }


}
