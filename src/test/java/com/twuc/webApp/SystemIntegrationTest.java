package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.contract.Zone;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class SystemIntegrationTest extends ApiTestBase {
    @Test
    void should_return_400_when_request_not_correct() throws Exception {
        mockMvc.perform(post("/api/staffs").contentType(MediaType.APPLICATION_JSON_UTF8).content(""))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_location_when_post_request() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(createStaff("Rob", "Hall"))
                .andExpect(status().is(201))
                .andReturn().getResponse();
        String location = response.getHeader("location");
        location.matches("^/api/staffs/[]0-9]+$");
    }

    private MockHttpServletRequestBuilder createStaff(String firstName, String lastName) throws JsonProcessingException {
        return post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(new CreateStaffRequest(firstName, lastName)));
    }

    @Test
    void should_get_different_location_when_post_two_requests() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse();
        MockHttpServletResponse anotherResponse = mockMvc.perform(createStaff("Rob", "Hide")).andReturn().getResponse();
        assertNotEquals(response.getHeader("location"), anotherResponse.getHeader("location"));
    }

    @Test
    void should_get_content_when_send_get_request() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");
        long expectedId = Long.parseLong(location.substring(location.lastIndexOf("/") + 1));
        mockMvc.perform(get(location))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.id").value(expectedId))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_return_404_when_staff_id_not_exit() throws Exception {
        mockMvc.perform(get("/api/staffs/2"))
                .andExpect(status().is(404));
    }

    @Test
    void should_get_staffs_when_send_get_request() throws Exception {
        mockMvc.perform(createStaff("Rob", "Hall"));
        mockMvc.perform(createStaff("Mary", "Sa"));
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.[0].firstName").value("Rob"))
                .andExpect(jsonPath("$.[0].lastName").value("Hall"))
                .andExpect(jsonPath("$.[1].firstName").value("Mary"))
                .andExpect(jsonPath("$.[1].lastName").value("Sa"));
    }

    @Test
    void should_get_void_array_when_no_staffs() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
                .andExpect(content().string("[]"));
    }

    @Test
    void should_return_200_when_update_success() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location + "/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_400_when_not_provide_zone_id() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location + "/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(new Zone(null))))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_zone_id_not_correct() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location + "/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"hello\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_zone_id_after_set_zone_id() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location + "/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));

        mockMvc.perform(get(location))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void should_get_zone_id_is_null_when_not_set() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "Hall")).andReturn().getResponse().getHeader("location");

        mockMvc.perform(get(location))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.zoneId").doesNotExist());
    }

    @Test
    void should_get_timezones_when_request() throws Exception {
        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0]").value("Africa/Abidjan"))
                .andExpect(jsonPath("$[7]").value("Africa/Bangui"));
    }

    @Test
    void should_return_201_when_create_reservation() throws Exception {
        mockMvc.perform(post("/api/staffs/2/reservations")
                .contentType(MediaType.APPLICATION_JSON_UTF8));
    }
}
