##1. Story 1

- ###AC1
  1. 给一个用户表stuff，当向"/api/staffs"发送POST请求，且发送的请求的content为Null，则响应的Status Code为400
  2. 给一个用户表stuff，当向"/api/staffs"发送POST请求，且发送的请求的content如示例所示，则响应的Status Code为201，且返回正确的Location
  3. 给一个用户表stuff, 两次发送POST请求返回的Location中的stuffId不相同

- ###AC2
  1. 创建一个用户，当发送正确的get请求时，会得到用户的信息
  2. 直接发送一个get请求，响应的Status Code为404
  
- ###AC3
  1. 创建两个用户，当向"/api/staffs"发送get请求时，会得到这两个用户的信息
  2. 不创建用户，当向"/api/staffs"发送请求时，会得到空的JSON数组

## 2. Story 2
- ###AC1
  1. 创建一个用户，向"/api/staffs/{staffId}/timezone"发送正确的请求（如示例），则响应的Status Code为200
  2. 创建一个用户，向"/api/staffs/{staffId}/timezone"发送请求时不提供zoneId，则响应为400
  3. 创建一个用户，向"/api/staffs/{staffId}/timezone"发送请求时zoneId名称不正确，则响应为400
- ###AC2
  1. 创建一个用户，并为其添加zoneId, 获得该用户信息时，返回的Content中包含zoneId的信息
  2. 创建一个用户，不为其添加zoneId, 获得该用户信息时，返回的Content中zoneId的值为null

## 3. Story 3
- ###AC1
  1. 向"/api/timezones"发送请求，可以获得时区列表
  
## 4. Story 4
- ###AC1
  1. 通过"/api/staffs/{staffId}/reservations"添加一个正确的预约，则响应Status Code 201和Location
  2. 通过"/api/staffs/{staffId}/reservations"添加预约，如果request不符合要求，则响应Status Code 400
  
  
